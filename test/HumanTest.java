import happyFamily.DayOfWeek;
import happyFamily.Human;
import org.junit.jupiter.api.Test;

public class HumanTest {

    Human testHuman;

    @Test
    public void testToStringMethod(){
        String[][] schedule = new String[][]{{DayOfWeek.MONDAY.name(), "Swimming"}};
        testHuman = new Human("John", "Silver", 1988, 55, schedule);

        String actual = testHuman.toString();
        String expected = "Human{name='John', surname='Silver', year=1988, iq=55, schedule=[[MONDAY, Swimming]]}";
    }
}
