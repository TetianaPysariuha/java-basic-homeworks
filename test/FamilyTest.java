import happyFamily.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class FamilyTest {
    Family testFamily;

    @BeforeEach
    public void setUp(){
        this.testFamily = new Family(new Human(), new Human());
    }

    @Test
    public void testCountFamily(){
        this.testFamily.addChild(new Human());
        int actual = testFamily.countFamily();
        int expected = 3;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testAddChild(){
        Human child = new Human("John", "Smith", 2020);
        this.testFamily.addChild(child);
        int actualChildrenLength = testFamily.getChildren().length;
        int expectedChildrenLength = 1;
        Assertions.assertEquals(expectedChildrenLength, actualChildrenLength);
        Assertions.assertEquals(testFamily.getChildren()[0], child);
    }

    @Test
    public void testDelChild(){
        Human childFirst = new Man("John", "Smith", 2020);
        Human childSecond = new Woman("Billy", "Smith", 2019);

        this.testFamily.addChild(childFirst);
        this.testFamily.addChild(childSecond);

        Human childForDel = new Human("John", "Smith", 2020);
        testFamily.delChild(childForDel);

        int actualChildrenLength = testFamily.getChildren().length;
        int expectedChildrenLength = 1;

        Assertions.assertEquals(expectedChildrenLength, actualChildrenLength);
        Assertions.assertEquals(testFamily.getChildren()[0], childSecond);
    }

    @Test
    public void testDelChildByIndex(){
        Human childFirst = new Man("John", "Smith", 2020);
        Human childSecond = new Woman("Billy", "Smith", 2019);

        this.testFamily.addChild(childFirst);
        this.testFamily.addChild(childSecond);

        testFamily.delChild(0);
        int actual = testFamily.indexChild(childFirst);
        int expected = -1;
        Assertions.assertEquals(expected, actual);

        int actualResultFind = testFamily.indexChild(childSecond);
        int expectedResultFind = 0;
    }

    @Test
    public void testToStringMethod(){
        String[][] williamScedual = {{DayOfWeek.MONDAY.name(), "call Endy"}};
        Human william = new Man("William", "Brown", 1960, 98, williamScedual);
        Human miriam = new Woman("Miriam", "Brown", 1965);
        Dog pet = new Dog("Rikky");
        this.testFamily = new Family(william, miriam, pet);
        Human childFirst = new Human("John", "Smith", 2020);
        testFamily.addChild(childFirst);

        String actual = testFamily.toString();
        String expected = "Family{mother=Human{name='William', surname='Brown', year=1960, iq=98, schedule=[[MONDAY, call Endy]]}, father=Human{name='Miriam', surname='Brown', year=1965, iq=0, schedule=null}, children[Human{name='John', surname='Smith', year=2020, iq=0, schedule=null}], pet=DOG{nickname='Rikky', age=0, trickLevel=0, habits=null}}";
    }
}
