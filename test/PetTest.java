import happyFamily.Dog;
import happyFamily.Pet;
import org.junit.jupiter.api.Test;

public class PetTest {

    Pet testPet;

    @Test
    public void testToStringMethod(){
        String[] habits = {"barking","chewing","pee at home"};
        testPet = new Dog("Rikki", 10, 53, habits);

        String actual = testPet.toString();
        String expected = "DOG{nickname='Rikki', age=10, trickLevel=53, habits=[barking, chewing, pee at home]}";
    };
}
