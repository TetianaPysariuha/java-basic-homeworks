package happyFamily;

import java.util.Arrays;
import java.util.Random;

public class Human {
    public Human() { }

    public Human(String humanName, String humanSurname, int humanYear){
        this.name = humanName;
        this.surname = humanSurname;
        this.year = humanYear;
    }

/*    public Human(String humanName, String humanSurname, int humanYear, Human humanMother, Human humanFather){
        this.name = humanName;
        this.surname = humanSurname;
        this.year = humanYear;
        this.mother = humanMother;
        this.father = humanFather;
    }*/

    public Human(String humanName, String humanSurname, int humanYear, /*Human humanMother, Human humanFather,*/ int humanIq, String[][] humanScadule){
        this.name = humanName;
        this.surname = humanSurname;
        this.year = humanYear;
/*        this.mother = humanMother;
        this.father = humanFather;*/
        this.iq = humanIq;
        this.schedule = humanScadule;
    }
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Human mother;
    private Human father;
    private String[][] schedule;
    private Family family;

    public String getName(){ return this.name; }
    public void setName(String newName){ this.name = newName; }

    public String getSurname(){ return this.surname; }
    public void setSurname(String newSurname){ this.surname = newSurname; }

    public int getYear(){ return this.year; }
    public void setYear(int newYear){ this.year = newYear; }

    public int getIq(){ return this.iq; }
    public void setIq(int newIq){ this.year = newIq; }

    public Pet getPet(){ return this.pet; }
    public void setPet(Pet newPet){ this.pet = newPet; }

    public Human getMother(){ return this.mother; }
    public void setMother(Human newMother){ this.mother = newMother; }

    public Human getFather(){ return this.father; }
    public void setFather(Human newFather){ this.father = newFather; }

    public String[][] getSchedule(){ return this.schedule; }
    public void setSchedule(String[][] newSchedule){ this.schedule = newSchedule; }

    public Family getFamily(){ return this.family; }
    public void setFamily(Family newFamily){ this.family = newFamily; }

    public void greetPet() {
        if(this.pet != null) {
            System.out.println("Привет, " + this.pet.getNickname());
        }
        else{
            System.out.println("Может стоит завести домашнего питомца? Будет с кем поговорить...");
        };
    }

    public void describePet() {
        if(this.pet != null){
            String trickLevel = this.pet.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый";
            System.out.format("У меня есть %s, ему %s лет, он %s", this.pet.getSpecies(), this.pet.getAge(), trickLevel);
        } else {
            System.out.println("У меня еще нет питомца... Но я уже подумываю его завести.");
        }
    }
    public boolean feedPet(boolean isItTimeToFeed) {
        if(this.pet != null){
            if(isItTimeToFeed){
                System.out.println("Хм... покормлю ка я " + this.pet.getNickname());
                return true;
            } else{
                Random random = new Random();
                if(random.nextInt(100) > this.pet.getTrickLevel()){
                    System.out.format("Думаю, %s не голоден.\n", this.pet.getNickname());
                    return false;
                }else{
                    System.out.println("Хм... покормлю ка я " + this.pet.getNickname());
                    return true;
                }
            }
        } else {
            System.out.println("Может стоит завести домашнего питомца? Было бы кого кормить...");
            return false;
        }
    }
    @Override
    public String toString() {
        return "Human{name='" + this.name
                + "', surname='" + this.surname
                + "', year=" + this.year
                + ", iq=" + this.iq
                /*+ ", mother=" + (this.mother == null ? null : this.mother.getName() + " " + this.mother.getSurname())
                + ", father=" + (this.father == null ? null : this.father.getName() + " " + this.father.getSurname())
                + ", pet=" + (this.pet == null ? null : this.pet.toString())*/
                + ", schedule=" + Arrays.deepToString(this.schedule)
                + "}";
    }
    @Override
    public boolean equals(Object obj){
        if (!(obj instanceof Human)) return false;
        Human p = (Human)obj;
        return this.name.equals(p.name) && this.surname.equals(p.surname) && this.year == p.year;
    }
    @Override
    public int hashCode(){
        return this.name.hashCode() + this.surname.hashCode() + this.year;
    }

    @Override
    protected void finalize ( ) {
        System.out.println("Deleted the object " + this.toString());
    }
}
