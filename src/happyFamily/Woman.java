package happyFamily;

public final class Woman extends Human{
    public Woman() { super(); }

    public Woman(String humanName, String humanSurname, int humanYear){
        super(humanName, humanSurname, humanYear);
    }

    public Woman(String humanName, String humanSurname, int humanYear, int humanIq, String[][] humanScadule) {
        super(humanName, humanSurname, humanYear, humanIq, humanScadule);
    }
    @Override
    public void greetPet() {
        if(this.getPet() != null) {
            System.out.println("Привет, малыш" + this.getPet().getNickname() + ". Как твои дела?");
        }
        else{
            System.out.println("Люблю котов... Может завести одного?");
        };
    }

    public void makeup() {
        System.out.println("Пора навести красоту...");
    }

}
