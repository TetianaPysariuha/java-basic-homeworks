package happyFamily;

public enum PetSpecies {
    Dog,
    Cat,
    Rabbit,
    Hamster,
    FISH,
    DomesticCat,
    RoboCat,
    Uknown
}
