package happyFamily;

public class UnknownPet extends Pet{
    public UnknownPet(){
        super();
        this.setSpecies(PetSpecies.Uknown);
    }

    public UnknownPet(String petName){
        super(petName);
        this.setSpecies(PetSpecies.Uknown);
    }

    public UnknownPet(String petName, int petAge, int petTrickLevel, String[] petHabits){
        super(petName, petAge, petTrickLevel, petHabits);
        this.setSpecies(PetSpecies.Uknown);
    }
    @Override
    public  void respond() {
        System.out.format("Я -%s. Рад тебя видеть хозяин.\n", this.getNickname());
    }
}
