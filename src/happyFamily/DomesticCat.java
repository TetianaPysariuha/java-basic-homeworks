package happyFamily;

public class DomesticCat extends Pet implements Foulable{
    public DomesticCat(){
        super();
        this.setSpecies(PetSpecies.DomesticCat);
    }

    public DomesticCat(String petName){
        super(petName);
        this.setSpecies(PetSpecies.DomesticCat);
    }

    public DomesticCat(String petName, int petAge, int petTrickLevel, String[] petHabits){
        super(petName, petAge, petTrickLevel, petHabits);
        this.setSpecies(PetSpecies.DomesticCat);
    }
    @Override
    public  void respond() {
        System.out.format("(%s потерлась о ноги хозяина.)\n", this.getNickname());
    }
    @Override
    public void foul(){
        System.out.println("Нужно хорошо замести следы...");
    }
}
