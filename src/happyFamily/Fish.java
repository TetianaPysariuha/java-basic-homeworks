package happyFamily;

public class Fish extends Pet{

    public Fish(){
        super();
        this.setSpecies(PetSpecies.FISH);
    }

    public Fish(String petName){
        super(petName);
        this.setSpecies(PetSpecies.FISH);
    }

    public Fish(String petName, int petAge, int petTrickLevel, String[] petHabits){
        super(petName, petAge, petTrickLevel, petHabits);
        this.setSpecies(PetSpecies.FISH);
    }
    @Override
    public  void respond() {
        System.out.format("(%s Молча подплыла и пристально посмотрела на хозяина.)\n", this.getNickname());
    }
}
