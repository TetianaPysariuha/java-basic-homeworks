package happyFamily;

public class RoboCat extends Pet{
    public RoboCat(){
        super();
        this.setSpecies(PetSpecies.RoboCat);
    }

    public RoboCat(String petName){
        super(petName);
        this.setSpecies(PetSpecies.RoboCat);
    }

    public RoboCat(String petName, int petAge, int petTrickLevel, String[] petHabits){
        super(petName, petAge, petTrickLevel, petHabits);
        this.setSpecies(PetSpecies.RoboCat);
    }
    @Override
    public void respond() {
        System.out.format("Хозяин, рад тебе безмерно! Lавай играть!)\n");
    }

}
