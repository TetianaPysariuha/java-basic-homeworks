package happyFamily;

import java.util.Arrays;

public class Family {

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[0];
        mother.setFamily(this);
        father.setFamily(this);
        mother.setPet(this.pet);
        father.setPet(this.pet);
    }
    public Family(Human mother, Human father, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[0];
        this.pet = pet;
        mother.setFamily(this);
        father.setFamily(this);
        mother.setPet(this.pet);
        father.setPet(this.pet);
    }
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Human getMother(){
        return this.mother;
    }
    public Human getFather(){
        return this.father;
    }
    public Human[] getChildren(){
        return this.children;
    }
    public Pet getPet(){
        return this.pet;
    }
    public void setPet(Pet newPet){
        this.pet = newPet;
    }
    public void addChild(Human newChild) {
        if(indexChild(newChild) >= 0){
            System.out.println("Such child already exist in this family");
            return;
        }
        Human[] newChildrenArray = new Human[this.children.length + 1];
        System.arraycopy(children, 0, newChildrenArray, 0,this.children.length);
        newChildrenArray[this.children.length] = newChild;
        this.children = newChildrenArray;
        newChild.setMother(this.mother);
        newChild.setFather(this.father);
        newChild.setFamily(this);
    }
    public int indexChild(Human child){
        for(int i = 0; i < this.children.length; i++) {
            if (this.children[i].equals(child)) {
                return i;
            }
        }
        return -1;
    }
    public boolean delChild(Human delChild) {
        int index = indexChild(delChild);
        if (index >= 0){
            if(delChild(index)){
                return true;
            } else {
                return false;
            }
        };
        System.out.println("The child does not found among children");
        return false;
    }

    public boolean delChild(int index){
        if(this.children.length <= index){
            return false;
        }
        Human delChild = this.children[index];
        this.children[index] = null;
        Human[] newChildren = new Human[this.children.length-1];
        for (int i = 0, j = 0; i < newChildren.length;){
            if(this.children[j] == null) {
                j++;
                continue;
            } else {
                newChildren[i] = this.children[j];
                i++;
                j++;
            }
        }
        this.children = newChildren;
        delChild.setFamily(null);
        delChild.setMother(null);
        delChild.setFather(null);
        return true;
    }
    public int countFamily(){
        return children.length + 2;
    }
    @Override
    public String toString() {
        return "Family{mother=" + this.mother.toString()
                + ", father=" + this.father.toString()
                + ", children" + Arrays.toString(this.children)
                + ", pet=" + (this.pet == null ? null : this.pet.toString())
                + "}";
    }
    @Override
    public boolean equals(Object obj){
        if (!(obj instanceof Family)) return false;
        Family p = (Family)obj;
        return this.mother.equals(p.mother) && this.father.equals(p.father);
    }
    @Override
    public int hashCode(){
        return this.mother.hashCode() + this.father.hashCode();
    }
    @Override
    protected void finalize ( ) {
        System.out.println("Deleted the object " + this.toString());
    }
}
