package happyFamily;

import java.util.Arrays;

public abstract class Pet {
    public Pet(){

    }

    public Pet(String petName){
        this.nickname = petName;
    }

    public Pet(String petName, int petAge, int petTrickLevel, String[] petHabits){
        this.nickname = petName;
        this.age = petAge;
        this.trickLevel = petTrickLevel;
        this.habits = petHabits;
    }

    private PetSpecies species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public PetSpecies getSpecies() {
        return this.species;
    }
    public void setSpecies(PetSpecies newSpecies) {
        this.species = newSpecies;
    }

    public String getNickname() {
        return this.nickname;
    }
    public void setNickname(String newNickname) {
        this.nickname = newNickname;
    }

    public int getAge() {
        return this.age;
    }
    public void setAge(int newAge) {
        this.age = newAge;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }
    public void setTrickLevel(int newTrickLevel) {
        if (newTrickLevel >= 0 && newTrickLevel <= 100){
            this.trickLevel = newTrickLevel;
        } else{
            System.out.println("Значенние trickLevel должно быть от 0 до 100");
        }
    }
    public String[] getHabits() {
        String[] newArray = new String[this.habits.length];
        System.arraycopy(this.habits, 0, newArray, 0, this.habits.length);
        return newArray;
    }
    public void setHabits(String[] newHabits) {
        if (habits == null || newHabits.length > this.habits.length) {
            this.habits = new String[newHabits.length];
        }
        System.arraycopy(newHabits, 0, this.habits, 0, this.habits.length);
    }
    public void eate() {
        System.out.println("Я кушаю!");
    }
    public abstract void respond();
    @Override
    public String toString(){
        return this.species
                + "{nickname='" + this.nickname
                + "', age=" + this.age
                + ", trickLevel=" + this.trickLevel
                + ", habits=" + Arrays.toString(habits)
                + "}";
    }
    @Override
    public boolean equals(Object obj){
        if (!(obj instanceof Pet)) return false;
        Pet p = (Pet)obj;
        return this.species.equals(p.species) && this.nickname.equals(p.nickname) && this.age == p.age;
    }
    public int hashCode(){
        return this.species.hashCode() + this.nickname.hashCode() + this.age;
    }
    @Override
    protected void finalize ( ) {
        System.out.println("Deleted the object " + this.toString());
    }
}
