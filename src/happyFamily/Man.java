package happyFamily;

public final class Man extends Human{
    public Man() { super(); }

    public Man(String humanName, String humanSurname, int humanYear){
        super(humanName, humanSurname, humanYear);
    }

    public Man(String humanName, String humanSurname, int humanYear, int humanIq, String[][] humanScadule){
        super(humanName, humanSurname, humanYear, humanIq, humanScadule);
    }
    @Override
    public void greetPet() {
        if(this.getPet() != null) {
            System.out.println("Привет, " + this.getPet().getNickname() + ". Хочешь поиграть?");
        }
        else{
            System.out.println("Всегда мечтал о собаке...");
        };
    }

    public void repairCar() {
        System.out.println("Опять что-то машина барахлит - пойду посмотрю, в чем дело...");
    }
}
