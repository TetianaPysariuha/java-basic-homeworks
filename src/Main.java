import happyFamily.*;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        System.out.println("\nAll about Pet ********************************\n");

        Dog myDog = new Dog("Rocky");
        System.out.println(myDog.toString());
        String[] habits = {"barking","chewing","pee at home"};
        myDog.setHabits(habits);
        System.out.println(myDog.toString());
        habits[0] = "roaring";
        myDog.setTrickLevel(45);
        System.out.println(Arrays.toString(habits));
        System.out.println(myDog.toString());

        myDog.eate();
        myDog.respond();
/*        myDog.foul();*/

        myDog.setTrickLevel(101);

        System.out.println("\nAll about Human ********************************\n");
        Human gerry = new Man("Gerry", "Smith", 1988);
        System.out.println(gerry.toString());
        String[][] schedule = new String[][]{{DayOfWeek.TUESDAY.name(), "training session"},{DayOfWeek.SUNDAY.name(), "training session"}};
        gerry.setSchedule(schedule);
        System.out.println(gerry.toString());
        gerry.greetPet();

        Human dorota = new Woman("Dorota", "Gasinska", 1990);

        System.out.println("\nAll about Family ********************************\n");

        Family family1 = new Family(dorota, gerry, myDog);
        gerry.greetPet();
        gerry.feedPet(false);
        family1.addChild(new Human("Billy", "Smith", 2020));
        System.out.println(family1.toString());
        family1.addChild(new Human("Billy", "Smith", 2020));

        System.out.println("\nAll about children ********************************\n");
        Human[] family1Children = family1.getChildren();
        System.out.println(family1Children[0].toString());
        family1Children[0].greetPet();
        family1Children[0].describePet();
        family1Children[0].feedPet(true);
        System.out.println("family2 has " + family1.countFamily() + " people");

        System.out.println("\nAll about another family ********************************\n");
        String[][] williamSchedule = {{DayOfWeek.MONDAY.name(), "call Endy"}};
        Human william = new Man("William", "Brown", 1960, 98, williamSchedule);
        Human miriam = new Woman("Miriam", "Brown", 1965);
        Family family2 = new Family(miriam, william);
        System.out.println(family2.toString());
        System.out.println("family2 has " + family2.countFamily() + " people");

        for(int i = 0; i < 1000000; i++) {
            Human testHuman = new Human("John" + i, "Smith", 1998+i);
        }

        Woman kate = new Woman("Kate", "Brown", 1958);
        kate.makeup();

        Man james = new Man("James", "Green", 1955);
        james.repairCar();
    }
}